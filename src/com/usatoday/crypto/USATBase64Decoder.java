package com.usatoday.crypto;
import sun.misc.BASE64Decoder;

/*
 * Created on Jul 23, 2004
 *
  * This class subclases the Sun's Base64Decoder. It overrides the bytes per line so that new lines
 * are not placed except for every 500000 characters.
 */

/**
 * @author aeast
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class USATBase64Decoder extends BASE64Decoder {

	/* (non-Javadoc)
	 * @see sun.misc.CharacterDecoder#bytesPerLine()
	 */
	protected int bytesPerLine() {
		return 500000;
	}

}
