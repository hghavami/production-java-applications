package com.usatoday.crypto;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;

/*
 * Created on Jul 1, 2004
 *
 * This is the base class for cryptography on the AS/400. It assumes a keystore has been created
 * in the current directory and that the public/private RSA key pair exists in the keystore.
 */

/**
 * Abstract Base class for cryptogoraphy prototype.
 * 
 * @author Andy East
 * @see AS400FieldLevelCryptoIntf
 * 
 */
public abstract class AS400CryptoBase {

    protected static String keystorePath = "./.keys";
    //protected static String keystorePath = "/home/aeast/crypto/.keys";

    protected static String keystoreName =  null;
    
    protected static final String privateKeyAlias = "usatoday" + "." + "midrange.com";
    
    protected String symmetricKeyAlias = "default_symkey";

    protected javax.crypto.SecretKey symmetricKey = null;

    private final String symmetricCryptoAlg = "DESede";

    private final String symmetricKeyAlg = "DESede";

    protected KeyStore keystore = null;

    protected PublicKey publicKey = null;

    protected Key privateKey = null;
    
    private String password = null;

    private boolean debugMode = false;
    
	/**
	 * Default constructor. Adds all necessary JCE providers.
	 */
	public AS400CryptoBase() {
		super();
		this.addProviders();
        
        // Look for location of keystore directory in the System environment if not set use default.
        String keyStoreDir = System.getProperty("com.usatoday.crypto.keyStoreDir");
        if (keyStoreDir != null && keyStoreDir.trim().length()  > 0 ) {
            keyStoreDir = keyStoreDir.trim();
            
            java.io.File f = new java.io.File(keyStoreDir);
            if (f.exists()) {
                if (f.isDirectory()){
                    // override default location
                    AS400CryptoBase.keystorePath = keyStoreDir;
                }
                else {
                    // error
                    System.out.println("INVALID KEYSTORE DIRECTORY (NOT A DIRECTORY) SPECIFIED IN JAVA ENV: com.usatoday.crypto.keyStoreDir=" + keyStoreDir);
                    System.out.println("DEFAULT KEYSTORE DIR WILL BE USED: " + AS400CryptoBase.keystorePath);
                }
            }
            else {
                // error
                System.out.println("INVALID KEYSTORE DIRECTORY (DOES NOT EXIST) SPECIFIED IN JAVA ENV: com.usatoday.crypto.keyStoreDir=" + keyStoreDir);
                System.out.println("DEFAULT KEYSTORE DIR WILL BE USED: " + AS400CryptoBase.keystorePath);
            }
        }
    }

	/**
	 * @return The name of the keystore.   NOT PART OF API
	 */
	protected String getKeystoreName() {
        if (AS400CryptoBase.keystoreName == null){
            AS400CryptoBase.keystoreName = AS400CryptoBase.keystorePath + "/usat_keystore.jks";
        }
		return keystoreName;
	}

    /**
     * Method to add the necessary JCE providers to the JVM - NOT PART OF API
     *
     */
	protected void addProviders() {
    
		try {
			Cipher.getInstance(this.getSymmetricCryptoAlg());
		} catch (Exception e) {
            if (this.isDebugMode()){
                System.out.println("JVM-DEBUG-MSG:: Installing SunJCE provider.");
            }
			Provider sunjce = new com.sun.crypto.provider.SunJCE();
			Security.addProvider(sunjce);	
		}
        	
        try {
            Cipher.getInstance("RSA");
        } catch (Exception e) {
            if (this.isDebugMode()){
                System.out.println("JVM-DEBUG-MSG:: Installing IBM JCE provider");
            }
            Provider ibmjce = new com.ibm.crypto.provider.IBMJCE();
            Security.addProvider(ibmjce); 
        }
	}


	/**
	 * @param string  
     * Set the alias to be used in symmetric cryptography for this instance.
     * If the symmetric key (Triple DES) does not already exist it will be created. 
     * EXTREME CARE SHOULD BE TAKEN TO ENSURE THAT THE KEY ALIAS IS SPECIFIED CORRECTLY.
     * FAILURE TO CORRECTLY SPECIFY THE KEY ALIAS MAY RESULT IN THE INABILITY TO DECRYPT DATA
     * OR CAUSE UNNECESSARY KEYS TO BE  GENERATED.
     * 
	 */
	public void setKeyAlias(String string) {
		if (string != null && string.trim().length() > 0) {
	
            this.symmetricKeyAlias = string.trim();
            this.symmetricKey = null;
		}
	}
    
    protected String getKeyAlias(){
        return this.symmetricKeyAlias;
    }

	/**
	 * @return The keystore object
	 */
	protected KeyStore getKeystore() {
		if (keystore == null) {
			try {
				this.keystore = KeyStore.getInstance(KeyStore.getDefaultType());
	
                java.io.FileInputStream fis =
                    new java.io.FileInputStream(this.getKeystoreName());
                
                if (this.password == null || this.password.length() == 0) {
                    if (this.isDebugMode()){
                        System.out.println(
                            "JVM-DEBUG-MSG:: Error opening keystore: Keystore Password not valid.");
                    }
                    keystore = null;
                }
                else {            
                    keystore.load(fis, this.password.toCharArray());
                    fis.close();
                }
			} catch (Exception e) {
                if (this.isDebugMode()){
                    System.out.println(
                        "JVM-DEBUG-MSG:: Error opening keystore: " + e.getMessage());
                }
				keystore = null;
			}
		}
		return keystore;
	}

	/**
	 * @return The private key
	 */
	protected Key getPrivateKey() {
		if (this.privateKey == null) {
			try {
                
				KeyStore ks = this.getKeystore();
	                
				privateKey =
					ks.getKey(AS400CryptoBase.getPrivateKeyAlias(), this.password.toCharArray());
			} catch (Exception e) {
                if (this.isDebugMode()){
    				System.out.println(
    					"JVM-DEBUG-MSG:: Unable to retrieve private key from store: ");
    				System.out.println("JVM-DEBUG-MSG:: Exception: " + e.getMessage());
                }
			}	
		}
		return privateKey;
	}

	/**
	 * @return The public asymmetric key for encryption
	 */
	protected PublicKey getPublicKey() {
		if (this.publicKey == null) {
			try {
	
				KeyStore ks = this.getKeystore();
	
				java.security.cert.Certificate cert =
					ks.getCertificate(AS400CryptoBase.getPrivateKeyAlias());
				if (cert == null) {
					throw new Exception(
						"Certificate is null for public key: "
							+ AS400CryptoBase.getPrivateKeyAlias());
				}
	
				this.publicKey = cert.getPublicKey();
			} catch (Exception e) {
                if (isDebugMode()){
    				System.out.println(
    					"JVM-DEBUG-MSG:: Exception getting public key: " + e.getMessage());
                }
			}
		}
		return publicKey;
	}

    /**
     * This method may be added to if we end up supporting encrypted and unencryped symmetric key storage.
     * @return
     */
    protected SecretKey getSecretKey() {
        if (this.symmetricKey != null){
            return this.symmetricKey;
        }
        else {
            return this.getSecretKeyFromEncryptedFile();
        }
    }
    
    /**
	 * @return NOT PART OF API
	 */
	protected String getSymmetricKeyFileName() {
        StringBuffer symKeyFileName = new StringBuffer(AS400CryptoBase.keystorePath + "/");
        
        symKeyFileName.append(this.symmetricKeyAlias);
        
        symKeyFileName.append(".ser");
        
		return symKeyFileName.toString();
	}


	/**
	 * @return   NOT PART OF API
	 */
	protected String getSymmetricCryptoAlg() {
		return symmetricCryptoAlg;
	}

	/**
	 * @return   NOT PART OF API
	 */
	protected String getSymmetricKeyAlg() {
		return symmetricKeyAlg;
	}

	/**
	 * @return boolean true or false if debug mode is on
	 */
	public boolean isDebugMode() {
		return debugMode;
	}

	/**
	 * @param b  NOT PART OF API
	 */
	protected void setDebugMode(boolean b) {
		debugMode = b;
	}
    
    /**
     * Method to enable debug mode. Debug mode simply sends information to the 
     * standard output for review to help in problem isolation
     *
     */
    public void enableDebug(){
        this.setDebugMode(true);
    }

    /**
     * Method to disable debug mode. Debug mode simply sends information to the 
     * standard output for review to help in problem isolation
     * Default debug mode is OFF (false)
     */
    public void disableDebug() {
        this.setDebugMode(false);
    }
	/**
	 * @return The name of the private key alias. NOT PART OF API
	 */
	protected static String getPrivateKeyAlias() {
		return AS400CryptoBase.privateKeyAlias;
	}
    
    /**
     * 
     * @return boolean if the secret key already exists. NOT PART OF API
     */
    private boolean secretKeyFileExists() {
        java.io.File f = new java.io.File(this.getSymmetricKeyFileName());
        
        if (f.exists()){
            if (this.isDebugMode()) {
                System.out.println("JVM-DEBUG-MSG:: secretKeyFileExists() - TRUE");
            }
            return true;
        }
        else {
            if (this.isDebugMode()) {
                System.out.println("JVM-DEBUG-MSG:: secretKeyFileExists() - FALSE");
            }
            return false;
        }
    }
    
    /**
     *  Generate a new secret key. NOT PART OF API
     *
     */
    private void generateSecretKey() {
        try {
            javax.crypto.KeyGenerator keygen =
                 javax.crypto.KeyGenerator.getInstance(this.getSymmetricKeyAlg());
        
            keygen.init(new SecureRandom());
            symmetricKey = keygen.generateKey();
                                    
            this.saveSecretKeyEncrypted();           
        }
        catch (NoSuchAlgorithmException nsa){
            if (this.isDebugMode()){
                System.out.println("JVM-DEBUG-MSG:: Unable to generate symmetric key: " + nsa.getMessage());
            }
            nsa.printStackTrace();
        }
    }
    
    protected String encodeBytes(byte[] bytes){
        String encodedStr = null;
        
        USATBase64Encoder encoder = new USATBase64Encoder();
        encodedStr = new String(encoder.encode(bytes));

        return encodedStr;
    }

    protected byte[] decodeText(String encodedText){
       byte[] decodedText = null;
        
       USATBase64Decoder decoder = new USATBase64Decoder();

       try {
           decodedText = decoder.decodeBuffer(encodedText);
       }
       catch (Exception e){
           if (isDebugMode()){
               System.out.println("JVM-DEBUG-MSG:: decodeText(): Failed to USATBase64 Decode string: " + e.getMessage());
               e.printStackTrace();
           }
           decodedText = null;
       }

       return decodedText;
    }

    protected void threadSleep(){
        try {
           // Thread.sleep(500);
        }
        catch (Exception e){
            
        }
    }
    
    /**
     * 
     * @return
     */
    private SecretKey getSecretKeyFromEncryptedFile() {
        try {
    
            Key pk = this.getPrivateKey();
            if (pk != null) {
    
                if (!this.secretKeyFileExists()){
                    this.generateSecretKey();
                }
                
                Cipher c1 = Cipher.getInstance(pk.getAlgorithm());
                c1.init(Cipher.DECRYPT_MODE, pk);
    
                CipherInputStream in =
                    new CipherInputStream(
                        new FileInputStream(this.getSymmetricKeyFileName()),
                        c1);
    
                byte[] tempByteArray = new byte[1024];
                int bytesRead = 0;
    
                bytesRead = in.read(tempByteArray);
    
                if (bytesRead > 1024) {
                    tempByteArray = new byte[4098];
                    in.reset();
                    bytesRead = in.read(tempByteArray);
    
                    if (bytesRead >= 4098) {
                        System.out.println("JVM-DEBUG-MSG:: Key file is too big");
                    }
                }
    
                in.close();
    
                byte[] sessionKeyArray = new byte[bytesRead];
    
                for (int i = 0; i < bytesRead; i++) {
                    sessionKeyArray[i] = tempByteArray[i];
                }

    
                DESedeKeySpec secretKeySpec =
                    new DESedeKeySpec(sessionKeyArray);
    
                SecretKeyFactory skf = SecretKeyFactory.getInstance(this.getSymmetricKeyAlg());
    
                this.symmetricKey = skf.generateSecret(secretKeySpec);
    
                return this.symmetricKey;
    
            } else {
                if (this.isDebugMode()) {
                    System.out.println(
                        "JVM-DEBUG-MSG:: The private key is not found: " + AS400CryptoBase.getPrivateKeyAlias());
                }
            }
    
        } catch (Exception e) {
            if (this.isDebugMode()){
                System.out.println(
                    "JVM-DEBUG-MSG:: Exception decrypting the session keys: " + e.getMessage());
                e.printStackTrace();
            }
        }
    
        return null;
    }
    
    /**
     * 
     *
     */
     private void saveSecretKeyEncrypted() {
        try {
    
            PublicKey pubKey = this.getPublicKey();
            if (pubKey != null) {
    
                Cipher c1 = Cipher.getInstance(pubKey.getAlgorithm());
                c1.init(Cipher.ENCRYPT_MODE, pubKey);
    
                new StringBuffer();
    
                CipherOutputStream out =
                    new CipherOutputStream(
                        new FileOutputStream(this.getSymmetricKeyFileName()),
                        c1);
        
                SecretKeyFactory keyfactory =
                        SecretKeyFactory.getInstance(this.getSymmetricKeyAlg());
                DESedeKeySpec keyspec =
                        (DESedeKeySpec) keyfactory.getKeySpec(
                            this.symmetricKey,
                            DESedeKeySpec.class);
                byte[] rawkey = keyspec.getKey();
    
                out.write(rawkey);
    
                out.close();
            } else {
                if (this.isDebugMode()){
                    System.out.println(
                        "JVM-DEBUG-MSG:: Save symmetric key:  The public key is not found: " + AS400CryptoBase.getPrivateKeyAlias());
                }
            }
    
        } catch (Exception e) {
            if (isDebugMode()){
                System.out.println(
                    "JVM-DEBUG-MSG:: Exception encrypting/storing the symmetric key: "
                        + e.getMessage());
                e.printStackTrace();
            }
        }
    }

	/**
	 * @param string
	 */
	public void setPassword(String string) {
        if (string == null || string.trim().length() == 0) {
            if (isDebugMode()){
                System.out.println(
                    "JVM-DEBUG-MSG:: Called setPassword with bogus string: '" + string + "'");
            }
        }
        else {
            password = new String(string.trim());
        }
	}

}
