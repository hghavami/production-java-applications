/*
 * Created on Aug 11, 2004
 *
 * This class is used to encrypt and decrypt strings. It is intended for Field level 
 * encryption/decryption on the AS/400
 * 
 */
package com.usatoday.crypto;

import java.security.Key;
import java.security.PublicKey;

import javax.crypto.Cipher;

/**
 *
 * This class is the implementation of the interface used by the AS/400 RPG applications for performing field level
 * encryption. Clients should refer to the interface defined in com.usatoday.crypto.AS400FieldLevelCryptoIntf.java
 * for the API to use.
 * 
 * It will perform encryption of plain text strings as well as decryption of USATBase64 Encoded Strings
 * Both symmmetric and asymmetric crytography are supported.
 * 
 * Symmetric Cryptography requires that the keyAlias be set prior to performing any encryption and decryption
 * If the named symmetric key does not exist then one will be generated.
 * 
 * @author Andy East
 * @see AS400CryptoBase
 * @see AS400FieldLevelCryptoIntf
 */
public class CryptoString extends AS400CryptoBase  implements AS400FieldLevelCryptoIntf {

    boolean useSymmetric = false;
 
	/**
	 * 
	 */
	public CryptoString() {
		super();
	}

    /**
     * @return
     */
    public boolean isUseSymmetric() {
        return useSymmetric;
    }
    
    /**
     *  Set Symmetric Mode
     */
    public void symmetricMode() {
        useSymmetric = true;
    }
    
    /**
     * Set Asymmetric mode
     */
    public void asymmetricMode() {
        useSymmetric = false;
    }
    
    /**
     * This method should return the cipher text or the plain text string "FAILED TO ENCRYPT" if encryption fails
     * 
     */
    public String encryptString(String plainText) {
        String cipherText = "FAILED TO ENCRYPT";

        if (plainText == null || plainText.length()==0){
            if (isDebugMode()){
                System.out.println("JVM-DEBUG-MSG:: encryptString() - Invalid plain text passed in. Text cannot be null or of zero length.");
            }
            return cipherText;
        }
        
        if (isDebugMode()) {
            System.out.println("JVM-DEBUG-MSG:: Entering  encryptString() - Encrypting string of length " + plainText.length() + ": '" + plainText + "'");
        }
        
        byte [] cipherBytes = this.encryptBytes(plainText.getBytes()); 
        
        if (cipherBytes != null && cipherBytes.length > 0) {
            String tempCipherText = this.encodeBytes(cipherBytes);
            
            if (tempCipherText != null){
                cipherText = tempCipherText;
            }    
        }
        
        if (this.isDebugMode()){
            System.out.println("JVM-DEBUG-MSG:: Leaving encryptString() - Returning cipher text of length " + cipherText.length() + ": '" + cipherText + "'");
            this.threadSleep();
            this.threadSleep();   
        }
        return cipherText;
    }
    
    /**
     * NOT PART OF  API
     * @param dataToEncrypt
     * @return - encrypted byte array
     */
    private byte[] encryptBytes(byte[] dataToEncrypt) {
        byte [] cipherBytes = null;
        try {

            byte [] data = null;
            data = dataToEncrypt;
            
            Key key = null;
            if (this.isUseSymmetric()) {
                key = this.getSecretKey();
            }
            else {
                PublicKey pk = this.getPublicKey();
                key = (Key)pk;
            }

            if (this.isDebugMode()){
                if (key == null) {
                    System.out.println("JVM-DEBUG-MSG:: encryptBytes() - KEY IS NULL");
                }
                else {
                    System.out.println("JVM-DEBUG-MSG:: encryptBytes() - Key retrieved: aglorithm: " + key.getAlgorithm());
                }
            }
            Cipher cipher = Cipher.getInstance(key.getAlgorithm());

            cipher.init(Cipher.ENCRYPT_MODE, key);

            cipherBytes = cipher.doFinal(data);

        } catch (Exception e) {
            if (this.isDebugMode()){
                System.out.println(
                    "JVM-DEBUG-MSG:: encryptBytes() - Failed to encrypt the string: "
                        + e.getClass().getName()
                        + "\tMessage:"
                        + e.getMessage());
            }
        }

        return cipherBytes;
    }   
    
    /**
     * Decrypt the string
     * 
     * @param String Cipher text to be decrypted (USATBase64 Encoded Cipher Text)
     * @return String The decryted string or the error message.
     */
    public String decryptString(String cipherText) {
        String errString = "FAILED TO DECRYPT";
        String returnStr = null;

        if (isDebugMode()) {
            System.out.println("JVM-DEBUG-MSG:: Entering decryptString() - Decrypting string of length " + cipherText.length() + ": '" + cipherText + "'");
      //      Charset chset = Charset.defaultCharset();
       //     System.out.println("Default charset: '" + chset.name() + "'");
        }
        
        if (cipherText == null || cipherText.length() == 0) {
            if (isDebugMode()){
                System.out.println("JVM-DEBUG-MSG:: Invalid string passed to decrypt method. Must not be null or of zero length.");
                System.out.println("JVM-DEBUG-MSG:: Leaving decryptString()");
            }
            return errString;
        }

        String errMessage = "";
        
        try {
            byte[] decodedText = this.decodeText(cipherText);
            
            if (decodedText == null){
                if (isDebugMode()){
                    System.out.println("JVM-DEBUG-MSG:: decryptString() - Failed to decode cipher text");
                    System.out.println("JVM-DEBUG-MSG:: Leaving decryptString()");
                }
                return errString;
            }
            
            if (isDebugMode()) {
                System.out.println("JVM-DEBUG-MSG:: Decoded string is " + decodedText.length + " bytes.");
            }
            
            byte[] decryptedBytes = this.decryptBytes(decodedText);
            
            if (decryptedBytes != null ) {
                byte [] data = decryptedBytes;
               
                returnStr = new String(data);
                // following works with Chris's
                //returnStr = new String(data, "UTF-16LE");
            }
           
        } catch (Exception e) {
            if (isDebugMode()){
                System.out.println( "JVM-DEBUG-MSG:: decryptString() - Problem decrypting encoded text: " + e.getMessage());
            }
            errMessage = e.getMessage();
        }
        finally {
            if (returnStr == null) {
                if (errMessage != null && errMessage.length() > 1) {
                    returnStr = errMessage;
                }
                else {
                    returnStr = errString;
                }
            }
            if (isDebugMode()) {
                System.out.println("JVM-DEBUG-MSG:: Leaving decryptString() - Returning decrypted text of length " + returnStr.length() + ": '" + returnStr + "'");
                this.threadSleep();
                this.threadSleep();
            }
        }
        
        return returnStr;
    }
    
    /**
     * 
     * @param cipherBytes - the encrypted byte array
     * @return The decrypted String
     */
    private byte[] decryptBytes(byte[] cipherBytes) {
        byte[] decryptedData = null;
        try {
              
            Key pk = null; 
            if (this.isUseSymmetric()) {
                pk = this.getSecretKey();
                if (pk == null) {
                    throw new Exception("Cannot locate secret key: " + this.getKeyAlias());
                }
            }
            else {
                pk =  this.getPrivateKey();             
                if (pk == null) {
                    throw new Exception("Cannot locate private key: " + AS400CryptoBase.getPrivateKeyAlias());
                }
            }  
            
            Cipher cipher = Cipher.getInstance(pk.getAlgorithm());
            cipher.init(Cipher.DECRYPT_MODE, pk);

            decryptedData = cipher.doFinal(cipherBytes);
            
        } catch (Exception e) {
            if (isDebugMode()){
                System.out.println(
                    "JVM-DEBUG-MSG:: decryptBytes() - Failed to decrypt: " + e.getMessage());
            }
        }

        return decryptedData;
    }
}
