/*
 * Created on Aug 11, 2004
 *
 * This class is a sample client of the AS/400 Cryptography library usat_crypto.jar.
 * 
 * You may use this as a sample for generating real applications.
 *  
 */
package com.usatoday.crypto.test;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import com.usatoday.crypto.CryptoString;

/**
 * @author aeast
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class TestCryptoString {

	/**
	 * 
	 */
	public TestCryptoString() {
		super();
	}
  
	public static void main(String[] args) {

        // uncomment to list registered JCE providers
        // TestCryptoString.listProviders();
        
        CryptoString scp = new CryptoString();
        
        // turn off debug
        scp.disableDebug();
        
        // specify the key that you plan to use for cryptography
        scp.setKeyAlias("my_test_key");
        
        // masked password reader not working
        //String password = PasswordField.readPassword("Enter password: ");
        
        // read key (not keystore) password from command line
        BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
        
        System.out.println("Enter Password: ");
        try {
            String password =  stdin.readLine();
            scp.setPassword(password);
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
        
        
        String message = null;
                     
        boolean done = false;
        boolean decryptAssym = false;
        boolean decryptSymm = false;
        
        // this loops until the user entera a 'q' or 'quit'
        while (!done) { 
            try {
               System.out.println("Enter String to encrypt('q' to quit, 'debug' to toggle debug): ");
               message = stdin.readLine();
                    
               if (message.equalsIgnoreCase("Q") || message.equalsIgnoreCase("quit")) {
                   done = true;
               }
               else if (message.equalsIgnoreCase("debug")){
                   if (scp.isDebugMode()){
                       scp.disableDebug();
                   }
                   else {
                       scp.enableDebug();
                   }
               }
               else if(message.equalsIgnoreCase("decrypt -a")){
                   decryptAssym = true;
               }
               else if(message.equalsIgnoreCase("decrypt -s")){
                   decryptSymm = true;
               }
               else {
                   message.length();
                   
                   if (decryptAssym) {
	                   scp.asymmetricMode();
	                   String decrypted = scp.decryptString(message);
                       System.out.println("Asymmetric Decrypted String: ");
                       System.out.println("'"+ decrypted + "'");
                       decryptAssym = false;
                   }
                   else if (decryptSymm){
	                   scp.symmetricMode();
	                   String decrypted = scp.decryptString(message);
                       System.out.println("Symmetrice Decrypted String: ");
                       System.out.println("'"+ decrypted + "'");
                       decryptSymm = false;
                   }
                   else {
	                   // first encrypt string with asymmetric key
	                   scp.asymmetricMode();
	                   String cipherText = scp.encryptString(message);
	                   System.out.println("A-SYMMETRIC RESULTS: '" + cipherText + "'");
	                   
	                   scp.decryptString(cipherText);
	                   
	                   // now use symmetric (3DES) to encrypt 
	                   scp.symmetricMode();
	                   cipherText = scp.encryptString(message);
	                   System.out.println("SYMMETRIC RESULTS: '" + cipherText + "'");
	                   scp.decryptString(cipherText);
	                   System.out.println();
                   }
               }
            }
            catch (Exception e) {
                System.out.println("Exception during processing: " + e.getMessage());
                e.printStackTrace();
                done = true;
            }
        }
	}
    
    public static void listProviders() {
        java.security.Provider[] providers = java.security.Security.getProviders();
        System.out.println("\n\nSecurity Providers: \n" );

        for (int i = 0; i < providers.length; i++) {
            System.out.println("\tProvider: " + providers[i].getInfo()); 
        }
    }
}
