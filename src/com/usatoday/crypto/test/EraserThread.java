/*
 * Created on Oct 5, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.usatoday.crypto.test;

/**
 * @author aeast
 *
 * The purpose of this class was to mask the input typed on the command line.
 * It doesn't work.
 */
public class EraserThread implements Runnable {

    private boolean stop;
 
       /**
        *@param The prompt displayed to the user
        */
       public EraserThread(String prompt) {
           System.out.print(prompt);
       }

       /**
        * Begin masking...display asterisks (*)
        */
       public void run () {
          stop = true;
          while (stop) {
             System.out.print("\010*");
         try {
             Thread.sleep(1);
             } catch(InterruptedException ie) {
                ie.printStackTrace();
             }
          }
       }

       /**
        * Instruct the thread to stop masking
        */
       public void stopMasking() {
          this.stop = false;
       }

}
