/*
 * Created on Oct 6, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.usatoday.crypto;

/**
 * @author aeast
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface AS400FileCryptoIntf extends AS400CryptoIntf {

    /**
     *  
     * @param sourceFile  the file to encrypt
     * @param destinationFile  - The file to save encrypted version to
     * @return true if successful, otherwise false
     */
    public boolean encryptFile(String sourceFile, String destinationFile);
    
    /**
     * 
     * @param sourceFile The source file, The original file will be overwritten 
     *                   with the encrypted version.
     * @return true if successful, otherwise false
     */
    public boolean encryptFile(String sourceFile);
    
    /**
     * 
     * @param sourceFile  File to Decrypt
     * @param destinationFile - File to store the decrypted contents
     * @return true if successful, otherwise false
     */
    public boolean decryptFile(String sourceFile, String destinationFile);
    
    /**
     * 
     * @param sourceFile - the file will be replaced with the decrypted contents
     * @return
     */
    public boolean decryptFile(String sourceFile);
    
    /**
     * 
     * @param fileToMigrate - The file to migrate to the new key
     * @param oldKeyAlias  -  The key used to originally encrypt the file
     * @param oldKeyPassword - password for old alias
     * @param newKeyAlias  - The new key alias to use for encryption
     * @param newKeyPassword - The password for the new key
     * @return
     */
    public boolean migrateEncryptedFileToNewAlias(String fileToMigrate, String oldKeyAlias, String oldKeyPassword, String newKeyAlias, String newKeyPassword);
}
