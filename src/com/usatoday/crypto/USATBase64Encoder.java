package com.usatoday.crypto;
import sun.misc.BASE64Encoder;

/*
 * Created on Jul 23, 2004
 *
 * This class subclases the Sun's Base64Encoder. It overrides the bytes per line so that new lines
 * are not placed except for every 500000 characters.
 */

/**
 * @author aeast
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class USATBase64Encoder extends BASE64Encoder {

	/**
	 * 
	 */
	public USATBase64Encoder() {
		super();
	}


	/* (non-Javadoc)
	 * @see sun.misc.CharacterEncoder#bytesPerLine()
	 */
	protected int bytesPerLine() {
		
		return 500000;
	}

}
