/*
 * Created on Oct 6, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.usatoday.crypto;

/**
 * @author aeast
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface AS400CryptoIntf {
    /**
     * 
     * @return True if in debug mode, otherwise false
     */
    public boolean isDebugMode();
   
    /**
     * Enbables debug messages
     *
     */
    public void enableDebug();
   
    /**
     * Disables debug messages
     *
     */
    public void disableDebug();
   
    /**
     * The password to access the private keys
     * @param password
     */
    public void setPassword(String password);
}
