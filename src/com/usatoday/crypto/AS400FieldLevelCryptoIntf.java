/*
 * Created on Aug 11, 2004
 *
 */
package com.usatoday.crypto;

/**
 * This interface represents the API for performing field level cryptography on the AS/400.
 * 
 * 
 * @author Andy East
 * @version 1.0
 * @since   1.0
 * 
 */
public interface AS400FieldLevelCryptoIntf extends AS400CryptoIntf {
   
   /**
    * 
    * @return True if in Symmetric mode, False if Asymmetric mode
    */ 
   public boolean isUseSymmetric();
   
   /**
    * Set the mode of operation (Toggle) to be symmetric cryptography
    *
    */ 
   public void symmetricMode();
    
   /**
    * Set the mode of operation (Toggle) to be asymmetric cryptography
    *
    */ 
   public void asymmetricMode();
   
   /**
    * 
    * @param plainText - The text to be encrypted
    * @return - The BASE 64 encoded cipher text, or "FAILED TO ENCRYPT if problem
    */ 
   public String encryptString(String plainText);
   
   /**
    * 
    * @param cipherText - The text to be decrypted
    * @return - The plain text or "FAILED TO DECRYPT" if problem
    */ 
   public String decryptString(String cipherText);
   
   /**
    * 
    * @param keyAlias - The name of the symmetric key to be used for cryptography.
    *                   If no symmetric key exists one will be created and saved for you.
    */
   public void setKeyAlias(String keyAlias);
   

}
