/*
 * Created on Jul 21, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.usatoday.apps.email;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.usatoday.apps.email.SmtpMailSender;

/**
 * @author aeast
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class EmailRootReport {

    private String notifyListFile = null;
    private String subject = "";
    private String contentFileName = null;
    private boolean deleteContentFile = false;
    private String mailServer = null;
   
    // log file support
    private Logger logger = null;
     
	public static void main(String[] args) {
        if (args.length == 0) {
            EmailRootReport.printUsage();
            System.exit(-1);
        }
        
        EmailRootReport emailReport = new EmailRootReport();
        
        emailReport.processArgs(args);
        
        emailReport.init();
        
        emailReport.doIt();
	}
    
    /**
     * 
     *
     */
    private void doIt() {
        SmtpMailSender mailer = new SmtpMailSender();
        
        if (this.getMailServer() != null) {
            mailer.setMailServerHost(this.getMailServer());
        }
        
        mailer.setMessageSubject(this.getSubject());
        mailer.addTORecipient(this.getRecipientList());
        mailer.setSender("RootReportMailer@usatoday.com");
        
        mailer.addAttachment(this.getContentFileName());
        mailer.setMessageText("Attached Extranet Root Report Sent At: " + (new Date(System.currentTimeMillis())).toString());
        
        try {
            mailer.sendMessage();
            this.logger.log(Level.INFO, "Root Report successfully sent to mail server.");
        }
        catch (Exception e) {
            this.logger.log(Level.SEVERE, "Exception sending root report: " + e.getMessage());
        }
        
        if (this.isDeleteContentFile()){
            java.io.File f = new java.io.File(this.getContentFileName());
            boolean deleted = f.delete();
            
            if (deleted) {
                this.logger.log(Level.INFO, "Root Report Attachment File Deleted");
            }
            else {
                this.logger.log(Level.INFO, "Root Report Attachment File FAILED to Delete");
            }
        }

        this.logger.log(Level.INFO, "Root Report Mailer Ending.");
        
    }
    
    /**
     * 
     * @return
     */
    private String getRecipientList() {
       StringBuffer recipientList = new StringBuffer();
       
       try {
           java.io.File f = new java.io.File(this.getNotifyListFile());
           if (f.exists()) {
               BufferedReader bir = new BufferedReader(new FileReader(f));
               
               String tempStr = null;
               while( (tempStr = bir.readLine()) != null) {
                   if (recipientList.length() > 0){
                       recipientList.append(", ");
                   }
                   recipientList.append(tempStr);
               }
               this.logger.log(Level.INFO, "Recipients: "+ recipientList.toString());
           }
           else {
               this.logger.log(Level.SEVERE, "Unable to open notificaiton file list: " + this.getNotifyListFile());
               this.logger.log(Level.SEVERE, "Using default address instead");
               recipientList.append("aeast@usatoday.com");
           }
       }
       catch (Exception e) {
           this.logger.log(Level.SEVERE, "Unable to open notificaiton file list: " + this.getNotifyListFile() + "  Exception: " + e.getMessage());
           this.logger.log(Level.SEVERE, "Using default address instead");
           recipientList.append("aeast@usatoday.com");
	   }
       
       return recipientList.toString(); 
    }
    
    /**
     * 
     *
     */
    public static void printUsage() {
        System.out.println("Useage: java.exe com.usatoday.apps.email.EmailRootReport [-delete] [-ms MailServerHost] -nl \"path to comma delimited notification list\" -s \"subject\" -f \"email attachment file\" ");
        System.out.println("           -delete option deletes the attachment file after sending the email");
        System.out.println("           -ms optionaly specify the mail server host. Otherwise it will be taken from property settings");
    }
    
    /**
     * 
     * @param args
     */
    protected void processArgs(String[] args) {
         for (int i = 0; i < args.length; i++) {
            
             String arg = args[i];
            
             if (arg.equalsIgnoreCase("-nl")) {
                 i++;
                 if (i >= args.length) {
                     EmailRootReport.printUsage();
                     System.exit(-1);
                 }
                 this.setNotifyListFile(args[i]);
             } else if (arg.equalsIgnoreCase("-s")) {
                 i++;
                 if (i >= args.length) {
                     EmailRootReport.printUsage();
                     System.exit(-1);
                 }
                 this.setSubject(args[i]);
             } else if (arg.equalsIgnoreCase("-f")) {
                 i++;
                 if (i >= args.length) {
                     EmailRootReport.printUsage();
                     System.exit(-1);
                 }
                 this.setContentFileName(args[i]);
             } else if (arg.equalsIgnoreCase("-ms")) {
                 i++;
                 if (i >= args.length) {
                     EmailRootReport.printUsage();
                     System.exit(-1);
                 }
                 this.setMailServer(args[i]);
             }
             else if (arg.equalsIgnoreCase("-delete")) {
                 this.setDeleteContentFile(true);
             }
             else {
                System.out.println("Unused argument: " + arg);
             }
         }
     }  
     
     /**
      * 
      *
      */
     private void init() {
         this.logger = Logger.getLogger("EmailRootReport");
         this.logger.setUseParentHandlers(false);
        
         // use system.out
         this.logger.addHandler(new ConsoleHandler());
        
         FileHandler f = null;
         try {
             f = new java.util.logging.FileHandler("Email_Root_Report.log", 50000, 1, true);
             f.setFormatter(new java.util.logging.SimpleFormatter());
                
             this.logger.addHandler(f);
         } catch (IOException e) {
             this.logger.log(Level.ALL, "Failed to initialize file logger");
         }

        this.logger.setLevel(Level.ALL);
        
        this.getLogger().log(Level.ALL, "Email Root Report Running");
     }


	/**
	 * @return
	 */
	public String getContentFileName() {
		return contentFileName;
	}

	/**
	 * @return
	 */
	public boolean isDeleteContentFile() {
		return deleteContentFile;
	}

	/**
	 * @return
	 */
	public Logger getLogger() {
		return logger;
	}

	/**
	 * @return
	 */
	public String getNotifyListFile() {
		return notifyListFile;
	}

	/**
	 * @return
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param string
	 */
	public void setContentFileName(String string) {
		contentFileName = string;
	}

	/**
	 * @param b
	 */
	public void setDeleteContentFile(boolean b) {
		deleteContentFile = b;
	}

	/**
	 * @param string
	 */
	public void setNotifyListFile(String string) {
		notifyListFile = string;
	}

	/**
	 * @param string
	 */
	public void setSubject(String string) {
		subject = string;
	}

	/**
	 * @return
	 */
	public String getMailServer() {
		return mailServer;
	}

	/**
	 * @param string
	 */
	public void setMailServer(String string) {
		mailServer = string;
	}

}

