package com.usatoday.apps.email;

import javax.mail.Message;
import javax.mail.Session;

/**
 * The <class>SmtpMailSender</class> class may be used to send Mime messages using the smtp
 * protocol. This class is dependent on the JavaMail extension and the JAVABEANS(tm) ACTIVATION 
 * FRAMEWORK. All mail exceptions are converted to IpsExceptions.<p>
 * Example Usage:<p>
 *		com.usatoday.esub.common.SmtpMailSender mailer = new com.usatoday.esub.common.SmtpMailSender();
 *		
 *		mailer.setSender( "theSender@anaddress.com" );
 *		mailer.setMailServerHost( "pop.xx.yyy.net" );
 *		
 *		mailer.setMessageSubject( "Test Message" );
 *		mailer.setMessageText( "Message Text" );
 *		
 *		// To recipients
 *		mailer.addTORecipient( "aRecipient@xxxx.com" );
 *
 *		// CC Recipients
 *		mailer.addCCRecipient( "anotherRecipient@xxxx.com" );
 *
 *		// BCC Recipients
 *		mailer.addBCCRecipient( "yetAnotherRecipient@yyyy.com" );
 *
 *		// Send the message	
 *		mailer.sendMessage();<p>
 *
 */
public class SmtpMailSender 
{	
	private String messageText = "default message";
	private java.util.ArrayList<String> toRecipients = null;
	private java.util.ArrayList<String> ccRecipients = null;
	private java.util.ArrayList<String> bccRecipients = null;
	private String sender = null;
	private String mailServerHost = null;
	private String subject = "";
	private java.util.Vector<String> fileAttachments = null;
    
    private String defaultCharSet = "utf-8";
    
/**
 * IpsSendMail constructor comment.
 */
public SmtpMailSender() {
	super();
}
/**
 * This method was created in VisualAge.
 * @param fileName java.lang.String
 */
public void addAttachment(String fileName) {

	if (fileName == null || fileName.length() == 0)
		return;
		
	if (this.fileAttachments == null) {
		this.fileAttachments = new java.util.Vector<String>();
	}

	this.fileAttachments.addElement( fileName );
}
/**
 * This method was created in VisualAge.
 * @param recipient java.lang.String
 */
public void addBCCRecipient(String recipient) 
{
	if (recipient == null)
	{
		throw new NullPointerException( "Recipient cannot be null");
	}
	getBccRecipients().add( recipient );
}
/**
 * This method was created in VisualAge.
 * @param recipient java.lang.String
 */
public void addCCRecipient(String recipient) 
{
	if (recipient == null)
	{
		throw new NullPointerException( "Recipient cannot be null");
	}
	getCcRecipients().add( recipient );
}
/**
 * This method was created in VisualAge.
 * @param message javax.mail.internet.MimeMessage
 */
private void addRecipients(javax.mail.internet.MimeMessage message) throws javax.mail.MessagingException
{
	// Add TO Recipients
	if (getToRecipients().size() > 0)
	{
		for( int i = 0; i < getToRecipients().size(); i++)
		{
			message.addRecipients( Message.RecipientType.TO, javax.mail.internet.InternetAddress.parse(getToRecipients().get(i)));
		}
	}
	// Add CC Recipients
	if (getCcRecipients().size() > 0)
	{
		for( int i = 0; i < getCcRecipients().size(); i++)
		{
			message.addRecipients( Message.RecipientType.CC, javax.mail.internet.InternetAddress.parse(getCcRecipients().get(i)));
		}
	}
	// Add BCC Recipients
	if (getBccRecipients().size() > 0)
	{
		for( int i = 0; i < getBccRecipients().size(); i++)
		{
			message.addRecipients( Message.RecipientType.BCC, javax.mail.internet.InternetAddress.parse(getBccRecipients().get(i)));
		}
	}
}
/**
 * This method was created in VisualAge.
 * @param recipient java.lang.String
 */
public void addTORecipient(String recipient) 
{
	if (recipient == null)
	{
		throw new NullPointerException( "Recipient cannot be null");
	}
	getToRecipients().add( recipient );
}
/**
 * This method was created in VisualAge.
 * @return java.util.Vector
 */
public java.util.Vector<String> getAttachments() {
	return this.fileAttachments;
}
/**
 * This method was created in VisualAge.
 * @return java.util.Vector
 */
private java.util.ArrayList<String> getBccRecipients() 
{
	if (bccRecipients == null)
	{
		bccRecipients = new java.util.ArrayList<String>();
	}
	return bccRecipients;
}
/**
 * This method was created in VisualAge.
 * @return java.util.Vector
 */
private java.util.ArrayList<String> getCcRecipients() 
{
	if (ccRecipients == null)
	{
		ccRecipients = new java.util.ArrayList<String>();
	}
	return ccRecipients;
}
/**
 * This method was created in VisualAge.
 * @return java.lang.String
 */
public String getMailServerHost() {
	return mailServerHost;
}
/**
 * This method was created in VisualAge.
 * @return java.lang.String
 */
public String getMessageSubject() {
	return subject;
}
/**
 * This method was created in VisualAge.
 * @return java.lang.String
 */
public String getMessageText() {
	return messageText;
}
/**
 * This method was created in VisualAge.
 * @return java.lang.String
 */
public String getSender() {
	return sender;
}
/**
 * This method was created in VisualAge.
 * @return java.util.Vector
 */
private java.util.ArrayList<String> getToRecipients() 
{
	if (toRecipients == null)
	{
		toRecipients = new java.util.ArrayList<String>();
	}
	return toRecipients;
}
/**
 * This method was created in VisualAge.
 * @param fileName java.lang.String
 */
public boolean removeAttachment(String fileName) {
	if (this.fileAttachments != null && fileName != null && this.fileAttachments.size() > 0) {
		return this.fileAttachments.removeElement(fileName);
	}

	return false;
}
/**
 * This method was created in VisualAge.
 * @param recipient java.lang.String
 */
public void removeBCCRecipient(String recipient) 
{
	if (getBccRecipients() != null && getBccRecipients().size() > 0)
	{
		for (int i = 0; i < getBccRecipients().size(); i++)
		{
			if (getBccRecipients().get(i).equals(recipient))
			{
				getBccRecipients().remove(i);
			}
		}
	}	
}
/**
 * This method was created in VisualAge.
 * @param recipient java.lang.String
 */
public void removeCCRecipient(String recipient) 
{
	if (getCcRecipients() != null && getCcRecipients().size() > 0)
	{
		for (int i = 0; i < getCcRecipients().size(); i++)
		{
			if (getCcRecipients().get(i).equals(recipient))
			{
				getCcRecipients().remove(i);
			}
		}
	}	
}
/**
 * This method was created in VisualAge.
 * @param recipient java.lang.String
 */
public void removeToRecipient(String recipient) 
{
	if (getToRecipients() != null && getToRecipients().size() > 0)
	{
		for (int i = 0; i < getToRecipients().size(); i++)
		{
			if (getToRecipients().get(i).equals(recipient))
			{
				getToRecipients().remove(i);
			}
		}
	}
}

/**
 * This method was created in VisualAge.
 */
public void sendMessage() throws java.io.IOException, NullPointerException, Exception
{
	if (getSender()==null)
	{
		throw new NullPointerException( "Sender cannot be null" );
	}

	javax.mail.Session session = null;
	
	try 
	{
		// Get a mail session
		try {
			javax.naming.InitialContext ic = new javax.naming.InitialContext();
			session = (Session) ic.lookup("smtp/mail");	 
			//System.out.println("Using J2EE SMTP Mail Provider");
		}
		catch (Exception e) {
			// Failed to find mail provider from J2EE provider so try to creat one
			java.util.Properties prop = new java.util.Properties();
		
			 //get the system properties
			if (this.getMailServerHost() != null)	{
				//System.out.println("Using client configured mail host: " + this.getMailServerHost());
				prop.put( "mail.smtp.host", this.getMailServerHost() );
			}
			else {
				prop = System.getProperties();
				//System.out.println("Using System properties for mail provider: " + System.getProperty("mail.smtp.host"));
			}
			session = Session.getDefaultInstance(prop, null);			
		}

		if (session == null) {
			throw new Exception("Unable to create a javax.mail.Session");
		}
	
		// create a message to send	
		javax.mail.internet.MimeMessage message = new javax.mail.internet.MimeMessage( session );

		message.setFrom( new javax.mail.internet.InternetAddress( getSender() ) );

		this.addRecipients(message);

		if (message.getAllRecipients() == null || message.getAllRecipients().length == 0)
		{
			throw new Exception( "At least one recipient is required to send." );
		}
		
		message.setSubject( getMessageSubject() );
	   	message.setSentDate(new java.util.Date());

        if (this.fileAttachments != null && this.fileAttachments.size() > 0) {
    	    // create the Multipart
    	    javax.mail.Multipart mp = new javax.mail.internet.MimeMultipart();
    	    
    	    // create and fill the first message part - text part
    	    javax.mail.internet.MimeBodyPart mbp1 = new javax.mail.internet.MimeBodyPart();
            
            mbp1.setText(getMessageText(), this.defaultCharSet);  
            
    	    // add the part to the multipart
    	    mp.addBodyPart(mbp1);
    
    	    for (int i = 0; this.fileAttachments != null && i < this.fileAttachments.size(); i++) {
    		    // create the second message part - attachment
    		    javax.mail.internet.MimeBodyPart mbp2 = new javax.mail.internet.MimeBodyPart();
                
                java.io.File f = new java.io.File(this.fileAttachments.elementAt(i));
                
    	            // attach the file to the message
    	   	    javax.activation.FileDataSource fds=new javax.activation.FileDataSource(this.fileAttachments.elementAt(i));
    		    mbp2.setDataHandler(new javax.activation.DataHandler(fds));
                
    		    mbp2.setFileName(f.getName());
    
    			mp.addBodyPart( mbp2 );
    	    }
    	    
    	    // Add the multipart message to the mail message
    	    message.setContent( mp );
        }
        else {
            message.setText(getMessageText(), this.defaultCharSet);
        }
	    
	 	javax.mail.Transport.send( message );

	} catch (javax.mail.MessagingException e)
	{
		// Convert exception to IpsException
		String s = e.toString();
		if (e.getNextException() != null) {
			Exception ex = e.getNextException();
			s = s.concat( "\n" + ex.toString() );
		}
		throw new Exception( s );		
	}
}
/**
 * This method was created in VisualAge.
 * @param newValue java.lang.String
 */
public void setMailServerHost(String newValue) {
	this.mailServerHost = newValue;
}
/**
 * This method was created in VisualAge.
 * @param newValue java.lang.String
 */
public void setMessageSubject(String newValue) {
	this.subject = newValue;
}
/**
 * This method was created in VisualAge.
 * @param newValue java.lang.String
 */
public void setMessageText(String newValue) {
	this.messageText = newValue;
}
/**
 * This method was created in VisualAge.
 * @param newValue java.lang.String
 */
public void setSender(String newValue) {
	this.sender = newValue;
}
/**
	 * @return
	 */
	public String getDefaultCharSet() {
		return defaultCharSet;
	}

	/**
	 * @param string
	 */
	public void setDefaultCharSet(String string) {
		defaultCharSet = string;
	}

}
