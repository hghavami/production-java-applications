/*
 * Created on Jan 30, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.covers;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.AreaAveragingScaleFilter;
import java.awt.image.BufferedImage;
import java.awt.image.CropImageFilter;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;

import javax.swing.ImageIcon;

/**
 * @author aeast
 * @date Jan 30, 2006
 * @class ImageManipulator
 * 
 * 
 * 
 */
public class ImageManipulator {
    
    public BufferedImage scaleImage(Image sourceImage, int width, int height)
    {
        //ImageFilter filter = new ReplicateScaleFilter(width,height);
        ImageFilter filter = new AreaAveragingScaleFilter(width, height);
        ImageProducer producer = new FilteredImageSource(sourceImage.getSource(),filter);
        Image resizedImage = Toolkit.getDefaultToolkit().createImage(producer);

        return this.toBufferedImage(resizedImage);
    }

    private BufferedImage toBufferedImage(Image image)
    {
        image = new ImageIcon(image).getImage();
        BufferedImage bufferedImage = new BufferedImage(image.getWidth(null),image.getHeight(null),BufferedImage.TYPE_INT_RGB);
        Graphics g = bufferedImage.createGraphics();
        g.setColor(Color.white);
        g.fillRect(0,0,image.getWidth(null),image.getHeight(null));
        g.drawImage(image,0,0,null);
        g.dispose();

        return bufferedImage;
    }

    public BufferedImage clipImage(Image sourceImage, int width, int height) {
        ImageFilter filter = new CropImageFilter(0,0,width,height);
        ImageProducer producer = new FilteredImageSource(sourceImage.getSource(),filter);
        Image resizedImage = Toolkit.getDefaultToolkit().createImage(producer);

        return this.toBufferedImage(resizedImage);
    }
}
