/*
 * Created on May 9, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.covers;

import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;
import java.net.URL;

import javax.swing.ImageIcon;

import org.joda.time.DateTime;

import com.usatoday.apps.email.SmtpMailSender;

/**
 * @author aeast
 * @date May 9, 2006
 * @class USATCoverImageProcessor
 * 
 * TODO Add a brief description of this class.
 * 
 */
public abstract class USATCoverImageProcessor {

    private float resizeScale = 50/100;
    private float clipScale = 50/100;
    
    protected String emailServer = "10.209.4.25";
    protected boolean emailImages = false;
    protected String emailReceivers = null;
    protected DateTime today = new DateTime();
	private String destinationFolder = "./resized";
    
    /**
     * 
     */
    public USATCoverImageProcessor() {
        super();
        
    }
    
    protected Image loadSourceImage(URL imageLocation) {
	      Image sourceImage = new ImageIcon(Toolkit.getDefaultToolkit().getImage(imageLocation)).getImage();
	      return sourceImage;
    }

    public float getResizeScale() {
        return this.resizeScale;
    }
    public void setResizeScale(String scaleStr) {
        
        this.resizeScale = Float.parseFloat(scaleStr)/100;
    }
    public float getClipScale() {
        return this.clipScale;
    }
    public void setClipScale(String clipScaleStr) {
        this.clipScale = Float.parseFloat(clipScaleStr)/100;;
    }
    public boolean isEmailImages() {
        return this.emailImages;
    }
    public String getEmailReceivers() {
        return this.emailReceivers;
    }
    public void setEmailImages(boolean emailImages) {
        this.emailImages = emailImages;
    }
    public void setEmailReceivers(String emailReceivers) {
        this.emailReceivers = emailReceivers;
    }
    
    /**
     * 
     * @param attachments
     */
    protected void sendEmail(File[] attachments, String extraText) {
        if (this.isEmailImages()) {
            try {
                SmtpMailSender mail = new SmtpMailSender();
                mail.setMailServerHost(this.emailServer);
                mail.setSender("hghavami@usatoday.com");
                mail.setMessageSubject( today.toString("MM/dd/yyyy") + " USA Today Cover Images" );
                if (extraText != null) {
                    mail.setMessageText("Attached are Images for " + today.toString("MM/dd/yyyy") + ". " + extraText);
                }
                else {
                    mail.setMessageText("Attached are Images for " + today.toString("MM/dd/yyyy"));
                }
                if (attachments != null) {
                    for(int i = 0; i < attachments.length; i++) {
                        mail.addAttachment(attachments[i].getAbsolutePath());
                    }
                }
                mail.addTORecipient(this.getEmailReceivers());
                mail.sendMessage();
            }
            catch (Exception e) {
                System.out.println("Failed to send emails. " + e.getMessage());
            }
        }
    }

    public String getEmailServer() {
        return this.emailServer;
    }
    public void setEmailServer(String emailServer) {
        this.emailServer = emailServer;
    }
    
    protected void processCommandLineArgs(String[] args) {
        for (int i = 0; i < args.length; i++) {
            if (args[i].equalsIgnoreCase("-sendEmail")) {
                int valueIndex = i + 1;
                if (valueIndex < args.length) {
                    String value = args[++i];
                    if (value.equalsIgnoreCase("true")) {
                        this.setEmailImages(true);
                    }
                }
            }
            else if (args[i].equalsIgnoreCase("-recipients")) {
                int valueIndex = i + 1;
                if (valueIndex < args.length) {
                    String value = args[++i];
                    this.setEmailReceivers(value);
                }
                else {
                    this.setEmailReceivers("aeast@usatoday.com");
                }
            }
            else if (args[i].equalsIgnoreCase("-emailServer")) {
                int valueIndex = i + 1;
                if (valueIndex < args.length) {
                    String value = args[++i];
                    this.setEmailServer(value);
                }
            }
        }
    }

	public String getDestinationFolder() {
		return destinationFolder;
	}

	public void setDestinationFolder(String destinationFolder) {
		this.destinationFolder = destinationFolder;
	}
}
