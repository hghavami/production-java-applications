/*
 * Created on May 9, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.covers;

import java.io.File;
import java.io.FilenameFilter;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;


/**
 * @author aeast
 * @date May 9, 2006
 * @class USATDirectoryCoverImageProcessor
 * 
 * 
 * 
 */
public class USATDirectoryCoverImageProcessor extends USATCoverImageProcessor {

    private String directoryPath = null;
    private boolean dynamicNames = false;
    private String fileNameFilter = null;
    /**
     * 
     */
    public USATDirectoryCoverImageProcessor() {
        super();
    }

    public static void main(String[] args) {
        USATDirectoryCoverImageProcessor processor = new USATDirectoryCoverImageProcessor();
        
      
       
       System.out.println("Directory Cover Image Processor running.");
       // process base class arguments
       processor.processCommandLineArgs(args);
       
       // process directory processor specific arguments
       for (int i = 0; i < args.length; i++) {
           if (args[i].equalsIgnoreCase("-dir")) {
               int valueIndex = i + 1;
               if (valueIndex < args.length) {
                   String value = args[++i];
                   processor.setDirectoryPath(value);
               }
           }

           if (args[i].equalsIgnoreCase("-dynamicNames")) {
        	   processor.setDynamicNames(true);
           }

           if (args[i].equalsIgnoreCase("-fileFilter")) {
               int valueIndex = i + 1;
               if (valueIndex < args.length) {
                   String value = args[++i];
                   if (value != null) {
                	   value = value.trim();
                	   if (value.length() > 0) {
                		   processor.setFileNameFilter(value);
                	   }
                   }                   
               }
           }

           if (args[i].equalsIgnoreCase("-destFolder")) {
               int valueIndex = i + 1;
               if (valueIndex < args.length) {
                   String value = args[++i];
                   if (value != null) {
                	   value = value.trim();
                	   if (value.length() > 0) {
                		   processor.setDestinationFolder(value);
                	   }
                   }                   
               }
           }
           
       }
       
       processor.doWork();
        
    }
    
    private File getLatestDirectory(File startDir) {
  	  File [] subDirs = startDir.listFiles();
	  
	  ArrayList<String> directoryNames = new ArrayList<String>();
	  HashMap<String, File>dirMap = new HashMap<String, File>();
	  if (subDirs != null && subDirs.length > 0) {
		  for (File f : subDirs) {
			  if (f.isDirectory()) {
				  directoryNames.add(f.getName()); // we sort on name
				  dirMap.put(f.getName(), f);      // but we really want the File object for processing 
			  }
		  }
	  }

	  if (directoryNames.size() > 0) {
		  Collections.sort(directoryNames);
		  // get Sub directory for year
		  startDir = dirMap.get(directoryNames.get(directoryNames.size() -1));
	  }
	  
	  return startDir;
	  
    }
    /**
     * 
     *
     */
    private void doWork() {
	    try
	    {
	      File directory = new File(this.getDirectoryPath());
	      
	      if (!directory.exists() || !directory.isDirectory()) {
	          System.out.println("The directory: " + directory.getAbsolutePath() + " does not exist. Terminating.");
	          return;
	      }

          if (this.isDynamicNames()) {

        	  // this assumes files are created by date such that a sort willl yeild the latest file last
        	  // It will go down 3 levels of directories if it can
        	  
        	  File subDirectory = this.getLatestDirectory(directory);

        	  while (!subDirectory.equals(directory)) {
        		  directory = subDirectory;
        		  subDirectory = this.getLatestDirectory(directory);
        	  }

          }

	      File[] imageFiles = directory.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
            	
                if (name.endsWith(".jpg")) {
                    return true;
                }
                else {
                    return false;
                }
            }
	      });

	      // filter out any prefixed files
	      if (this.getFileNameFilter() != null && this.getFileNameFilter().length() > 0) {
	    	  ArrayList<File> newList = new ArrayList<File>();
	    	  for (File f : imageFiles) {
	    		  if (f.getName().startsWith(this.getFileNameFilter())) {
	    			  newList.add(f);
	    		  }
	    	  }
	    	  
	    	  imageFiles = new File[newList.size()];
	    	  for (int i = 0; i < newList.size(); i++) {
	    		  imageFiles[i] = newList.get(i);
	    	  }
	      }
	      
	      File newDir = null;
	      if (imageFiles != null && imageFiles.length > 0) {
	          System.out.println("There are " + imageFiles.length + " image files to be processed.");
		      // create a resize directory
	          newDir = new File(this.getDestinationFolder());
	          if (!newDir.exists()) {
	              newDir.mkdir();
	          }
	      }
	      else {
	          System.out.println("There are no .jpg files in the source folder: " + directory.getAbsolutePath());
	      }

	      for(int i = 0; i < imageFiles.length; i++) {
	          
	          File currentFile = imageFiles[i];
	          URI fileURI = currentFile.toURI();
	          URL fileURL = fileURI.toURL();
	          UsatURLCoverImageProcessor urlProcessor = new UsatURLCoverImageProcessor();
	          
	          urlProcessor.setEmailImages(this.emailImages);
	          urlProcessor.setEmailReceivers(this.emailReceivers);
	          urlProcessor.setEmailServer(this.emailServer);
	          urlProcessor.setUrl(fileURL.toString());
	          urlProcessor.setResizeScale("70");  // 70 % is default
	          urlProcessor.setClipScale("50"); // clip it in half
	          urlProcessor.setDestinationFolder(this.getDestinationFolder());
	          
	          urlProcessor.processImage();
	          
/*	          
		      FileOutputStream outfile = new FileOutputStream(new File( newDir.getAbsolutePath() + "/" + imageFiles[i].getName().substring(0,imageFiles[i].getName().indexOf('.')) + "TN.jpg"));
		      JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(outfile);
		      encoder.encode(resizedImage);
		      
		      
		      // Main Image 
		      sourceImage = new ImageIcon(Toolkit.getDefaultToolkit().getImage(imageFiles[i].getAbsolutePath())).getImage();
	
		      targetWidth = 327;
		      targetHeight = 570;
		      
		      resizedImage = this.scaleImage(sourceImage,targetWidth,targetHeight);
	
		      outfile = new FileOutputStream(new File( newDir.getAbsolutePath() + "/" + imageFiles[i].getName().substring(0,imageFiles[i].getName().indexOf('.')) + "M.jpg"));
		      encoder = JPEGCodec.createJPEGEncoder(outfile);
		      encoder.encode(resizedImage);
		      */
	      }
	    }
	    catch(Exception e)
	    {
	        System.out.println("Exception Processing: " + e.getMessage());
	        e.printStackTrace();
	    }
    }
    
    String getDirectoryPath() {
        return this.directoryPath;
    }
    void setDirectoryPath(String directoryPath) {
        this.directoryPath = directoryPath;
    }

	public boolean isDynamicNames() {
		return dynamicNames;
	}

	public void setDynamicNames(boolean dynamicNames) {
		this.dynamicNames = dynamicNames;
	}

	public String getFileNameFilter() {
		return fileNameFilter;
	}

	public void setFileNameFilter(String fileNameFilter) {
		this.fileNameFilter = fileNameFilter;
	}
}
