/*
 * Created on May 9, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.covers;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import org.joda.time.DateTime;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;

/**
 * @author aeast
 * @date May 9, 2006
 * @class UsatCoverImageProcessor
 * 
 * This class processes a single file from a web location and writes it to the current directory.
 * If the main class is called, it can be used to pull the current date's image from the usatoday.com web site.
 * 
 */
public class UsatURLCoverImageProcessor extends USATCoverImageProcessor {

    private String url = null;
    private URL sourceURL = null;
    
    // known name formats
    // http://images.usatoday.com/printedition/images/A01_06_15_2006_4FINAL_H1.jpg     _6FACHASE_H1   _6FACHASE_RO.jpg
    String changingPart [] = { "_1FINAL_RO.jpg", "_2FINAL_RO.jpg", "_3FINAL_RO.jpg", "_4FINAL_RO.jpg", "_5FINAL_RO.jpg",  "_6FINAL_RO.jpg",
							  "_1CHASE_RO.jpg","_2CHASE_RO.jpg", "_3CHASE_RO.jpg", "_4CHASE_RO.jpg", "_5CHASE_RO.jpg", "_6CHASE_RO.jpg",
							  "_1FACHASE_RO.jpg", "_2FACHASE_RO.jpg", "_3FACHASE_RO.jpg", "_4FACHASE_RO.jpg", "_5FACHASE_RO.jpg", "_6FACHASE_RO.jpg", 
							  "_1FA_CHASE_RO.jpg", "_2FA_CHASE_RO.jpg", "_3FA_CHASE_RO.jpg", "_4FA_CHASE_RO.jpg", "_5FA_CHASE_RO.jpg", "_6FA_CHASE_RO.jpg",
    						  "_1FINAL_H1_RO.jpg", "_2FINAL_H1_RO.jpg", "_3FINAL_H1_RO.jpg", "_4FINAL_H1_RO.jpg", "_5FINAL_H1_RO.jpg",  "_6FINAL_H1_RO.jpg",
							  "_1CHASE_H1_RO.jpg","_2CHASE_H1_RO.jpg", "_3CHASE_H1_RO.jpg", "_4CHASE_H1_RO.jpg", "_5CHASE_H1_RO.jpg", "_6CHASE_H1_RO.jpg",
							  "_1FACHASE_H1_RO.jpg", "_2FACHASE_H1_RO.jpg", "_3FACHASE_H1_RO.jpg", "_4FACHASE_H1_RO.jpg", "_5FACHASE_H1_RO.jpg", "_6FACHASE_H1_RO.jpg", 
							  "_1FA_CHASE_H1_RO.jpg", "_2FA_CHASE_H1_RO.jpg", "_3FA_CHASE_H1_RO.jpg", "_4FA_CHASE_H1_RO.jpg", "_5FA_CHASE_H1_RO.jpg", "_6FA_CHASE_H1_RO.jpg",
    						  "_1FINAL_H1.jpg", "_2FINAL_H1.jpg", "_3FINAL_H1.jpg", "_4FINAL_H1.jpg", "_5FINAL_H1.jpg",  "_6FINAL_H1.jpg",
    						  "_1CHASE_H1.jpg","_2CHASE_H1.jpg", "_3CHASE_H1.jpg", "_4CHASE_H1.jpg", "_5CHASE_H1.jpg", "_6CHASE_H1.jpg",
    						  "_1FACHASE_H1.jpg", "_2FACHASE_H1.jpg", "_3FACHASE_H1.jpg", "_4FACHASE_H1.jpg", "_5FACHASE_H1.jpg", "_6FACHASE_H1.jpg", 
    						  "_1FA_CHASE_H1.jpg", "_2FA_CHASE_H1.jpg", "_3FA_CHASE_H1.jpg", "_4FA_CHASE_H1.jpg", "_5FA_CHASE_H1.jpg", "_6FA_CHASE_H1.jpg",
    						  "_1FINAL.jpg", "_2FINAL.jpg", "_3FINAL.jpg", "_4FINAL.jpg", "_5FINAL.jpg",  "_6FINAL.jpg",
    						  "_1CHASE.jpg","_2CHASE.jpg", "_3CHASE.jpg", "_4CHASE.jpg", "_5CHASE.jpg", "_6CHASE.jpg",
    						  "_1FACHASE.jpg", "_2FACHASE.jpg", "_3FACHASE.jpg", "_4FACHASE.jpg", "_5FACHASE.jpg", "_6FACHASE.jpg", 
    						  "_1FA_CHASE.jpg", "_2FA_CHASE.jpg", "_3FA_CHASE.jpg", "_4FA_CHASE.jpg", "_5FA_CHASE.jpg", "_6FA_CHASE.jpg"};  
    /**
     * 
     */
    public UsatURLCoverImageProcessor() {
        super();
    }

    /**
     * 
     * @param args The command line arguments
     */
    public static void main(String[] args) {
        
       UsatURLCoverImageProcessor processor = new UsatURLCoverImageProcessor();
        
       System.out.println("URL Cover Image Processor running.");
       String urlOverride = null;
       
       processor.processCommandLineArgs(args);
       
       for (int i = 0; i < args.length; i++) {
           if (args[i].equalsIgnoreCase("-url")) {
               int valueIndex = i + 1;
               if (valueIndex < args.length) {
                   String value = args[++i];
                   urlOverride = value;
               }
           }
       }
       
       try {
           if (urlOverride != null) {
               processor.setUrl(urlOverride);
		       System.out.println("Processing Image file: " + urlOverride);
           }
           else {
               
               processor.determineCoverImageName();
		       
           }
	       processor.setResizeScale("70");  // 70 % is default
	       processor.setClipScale("50"); // clip it in half
	       
	       processor.processImage();
	       
       }
       catch (Exception e) {
           e.printStackTrace();
           System.out.println("Exception Message: " + e.getMessage());
           processor.sendEmail(null, "Exception Processing Images - possible holiday: " + e.getMessage());
       }
       
       System.out.println("Application terminating.");
    }
    
    /**
     * this method processes the image for the specified URL
     *
     */
    public void processImage() throws Exception {
        
        if (this.getUrl() == null) {
            throw new Exception("No URL Specified for processing.");
        }

        Image image = this.loadSourceImage(this.sourceURL);

        // First resize the image
	    int targetWidth = (int)(image.getWidth(null)*this.getResizeScale());
	    int targetHeight = (int)(image.getHeight(null)*this.getResizeScale());
        
	    ImageManipulator m = new ImageManipulator();
	    
        BufferedImage resizedImage = m.scaleImage(image, targetWidth, targetHeight);

        // save scaled cover image.
        File mainImage = this.saveMainImage(resizedImage);
        
        // now clip and resize the image.
        targetWidth = image.getWidth(null);
        targetHeight = (int)(image.getHeight(null)*this.getClipScale());
        
        resizedImage = m.clipImage(image, targetWidth, targetHeight);
        
        // set new scale
        this.setResizeScale("40"); // only want 40 % of original
	    targetWidth = (int)(resizedImage.getWidth(null)*this.getResizeScale());
	    targetHeight = (int)(resizedImage.getHeight(null)*this.getResizeScale());
	    
	    resizedImage = m.scaleImage(resizedImage, targetWidth, targetHeight);
	    
	    File thumbnailImage = this.saveThumbnailImage(resizedImage);
	    
	    if (this.emailImages && this.emailReceivers != null) {
	        File [] attachments = new File[2];
	        attachments[0] = mainImage;
	        attachments[1] = thumbnailImage;
	        this.sendEmail(attachments, null);
	    }

    }
    
    /**
     * 
     * @return
     */
    String getUrl() {
        return this.url;
    }
    
    /**
     * 
     * @param url
     * @throws Exception
     */
    void setUrl(String url) throws Exception {
        this.url = url;
        this.sourceURL = new URL(url);
        
        URLConnection conn = this.sourceURL.openConnection();
        if (conn != null && conn instanceof HttpURLConnection) {
            HttpURLConnection httpConn = (HttpURLConnection)conn;
            if (httpConn.getResponseCode() != HttpURLConnection.HTTP_OK)  {
                throw new Exception("Unable to find specified resource: " + url);
            }
        }
    }
    
    public void determineCoverImageName() throws Exception {
       DateTime today = new DateTime();
       String part1 = "http://i.usatoday.net/printedition/images/A01_";
       
       String part2 = today.toString("MM_dd_yyyy");

       Exception e = null;
       
       boolean found = false;
       for (int i=0; !found && i < this.changingPart.length; i++) {
           String urlStr = part1 + part2 + this.changingPart[i]; 
           try {
               this.setUrl(urlStr);
               found = true;
           }
           catch (Exception exp) {
               System.out.println("Failed to locate image: " + urlStr + ". Attempting alternate.");
               e = exp;
           }           
       }
       
       if (!found && e != null) {
           throw e;
       }
       else {
           System.out.println("Found Image: " + this.getUrl());
       }
       
    }
    
    private File saveMainImage(BufferedImage image) throws Exception {
        
        File fd = new File(this.getDestinationFolder());
        if (!fd.exists()) {
            fd.mkdir();
        }
        
        String fileName = this.sourceURL.getPath();
        int index = fileName.lastIndexOf("/");
        
        if (index > -1 ) {
        	if (index < fileName.length()) {
            	fileName = fileName.substring(index+1);
        	}
        	else {
            	fileName = fileName.substring(index);
        	}
        	
        	// remove any extension
            int indexOfExtension = fileName.lastIndexOf(".");
            if (indexOfExtension > -1) {
            	fileName = fileName.substring(0, indexOfExtension);
            }
        }
        
        File outFileName = new File(this.getDestinationFolder() + File.separator + fileName + "_M.jpg");
        // always replace any existing file in the resized folder.
        if (outFileName.exists()) {
            outFileName.delete();
        }
        
		FileOutputStream outfile = new FileOutputStream(outFileName);
		JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(outfile);
		encoder.encode(image);
		
		return outFileName;
    }
    
    private File saveThumbnailImage(BufferedImage image) throws Exception {
        
        File fd = new File(this.getDestinationFolder());
        if (!fd.exists()) {
            fd.mkdir();
        }
        
        String fileName = this.sourceURL.getPath();
        int index = fileName.lastIndexOf("/");
        
        if (index > -1) {
        	if (index < fileName.length()) {
            	fileName = fileName.substring(index+1);
        	}
        	else {
            	fileName = fileName.substring(index);
        	}
        	// remove any extension
            int indexOfExtension = fileName.lastIndexOf(".");
            if (indexOfExtension > -1) {
            	fileName = fileName.substring(0, indexOfExtension);
            }
       }
       
        File outFileName = new File(this.getDestinationFolder() + File.separator + fileName + "_TN.jpg");
        // always replace any existing file in the resized folder.
        if (outFileName.exists()) {
            outFileName.delete();
        }
        
		FileOutputStream outfile = new FileOutputStream(outFileName);
		JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(outfile);
		encoder.encode(image);
		
		return outFileName;
    }

}
