
package com.usatoday.nexternal.integration;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

public class NexternalIntegration {

//	Test Website
 // public static String nexternalURL = "https://www.nexternal.com/staging/default.asp";
 
//	Production Website
 	public static String nexternalURL = "https://www.nexternal.com/usatoday/default.aspx";
	
//  Location of Nexternal_Integration directory
 	public static String base_URL = "D:\\Nexternal_Integration\\";
//	public static String base_URL = "C:\\Nexternal_Integration\\";

//  400 files
	public static String updateCustomerFile =  "C:\\inetpub\\ftproot\\nextftp\\outgoing\\Customer.txt";
//	public static String updateCustomerFile =  "C:\\Nexternal_Integration\\outgoing\\Customer.txt";

public static void main(String[] args)throws IOException {	 {		
		 new StringBuffer();  
		 		 
		 boolean updFileExists = (new File(updateCustomerFile)).exists();
		 if (updFileExists) {
		        sendToNexternal(updateCustomerFile, "UPDATE");
		 } else {
		 	  new BufferedWriter(new FileWriter(base_URL + "Logs\\LogFile_For_Updates_Not_Found_"  + getDateFormatted() + ".txt"));
				
		 	
		 } 
		 
	 }		
}

public static void sendToNexternal(String processFile, String processType)throws IOException {	 {
	 
	 String processedfile = base_URL +"Processed_Files\\Completed_" + processType + "_" + getDateFormatted() + "_.txt";
	 String outfilename = base_URL + "Logs\\LogFile_For_" + processType + "_" + getDateFormatted() + ".txt"  ;
	
	 new StringBuffer(); 	   	
	 BufferedReader in = new BufferedReader(new FileReader(processFile));
	 BufferedWriter buf_writer = new BufferedWriter(new FileWriter(outfilename));
	 
 	 buf_writer.write( "        Nexternal Intergration for Website: " + nexternalURL + " File= " + processFile); 
	 buf_writer.newLine();
	 
	 
	 
	 while (true) {
	   String s = in.readLine();
	   if (s == null) {
		   break;
	   }
	   java.util.StringTokenizer stringTokenizer= new java.util.StringTokenizer(s,"||"); 
	   int index = 0; 
	   int numTokens = stringTokenizer.countTokens(); 
	   
	   String[] A = new String[numTokens]; 
	   if(numTokens>0) 
	    { 
	       while(stringTokenizer.hasMoreTokens()) 
	       { 
	          String token = stringTokenizer.nextToken(); 
	          A[index++] = token;
	       } 
	    } 
	  
	   List <NameValuePair> nvps = new ArrayList <NameValuePair>();
      nvps.add(new BasicNameValuePair("LAST_NAME", removeQuotes(A[1])));
      nvps.add(new BasicNameValuePair("FIRST_NAME", removeQuotes(A[2])));
      nvps.add(new BasicNameValuePair("EMAIL", removeQuotes(A[3])));
      nvps.add(new BasicNameValuePair("ADDRESS1", removeQuotes(A[5])));
      nvps.add(new BasicNameValuePair("ADDRESS2", removeQuotes(A[6])));
      nvps.add(new BasicNameValuePair("CITY", removeQuotes(A[7])));
      nvps.add(new BasicNameValuePair("STATE", removeQuotes(A[8])));
      nvps.add(new BasicNameValuePair("POSTAL_CODE", removeQuotes(A[9])));
      nvps.add(new BasicNameValuePair("COUNTRY", removeQuotes(A[10])));
      nvps.add(new BasicNameValuePair("PHONE_NUM", removeQuotes(A[12])));
      nvps.add(new BasicNameValuePair("MAILING_LIST", "0"));
      nvps.add(new BasicNameValuePair("CUST_TYPE", removeQuotes(A[19])));
      if ((A[19]) != null) {
   	   nvps.add(new BasicNameValuePair("PASSWORD", removeQuotes(A[20])));
          }
      nvps.add(new BasicNameValuePair("CUSTOM_FIELD1", removeQuotes(A[22])));
      nvps.add(new BasicNameValuePair("CUSTOM_FIELD2", removeQuotes(A[23])));
      nvps.add(new BasicNameValuePair("CUSTOM_FIELD3", removeQuotes(A[24])));
      nvps.add(new BasicNameValuePair("CUSTOM_FIELD4", removeQuotes(A[25])));
      nvps.add(new BasicNameValuePair("CUSTOM_FIELD5", removeQuotes(A[26])));
      nvps.add(new BasicNameValuePair("CUSTOM_FIELD6", removeQuotes(A[27])));
      nvps.add(new BasicNameValuePair("Match1", "CUSTOM_FIELD1"));

      nvps.add(new BasicNameValuePair("Mode", "Display-Only"));
      nvps.add(new BasicNameValuePair("Submit", "Shop Online"));
	 
   ///////////// NEW 
   DefaultHttpClient httpclient = new DefaultHttpClient();
      
   HttpPost httppost = new HttpPost(nexternalURL);
  	httppost.setHeader("Content-Type", "application/x-www-form-urlencoded");
  	httppost.setHeader("REFERER", "http://service.usatoday.com");
  	httppost.setHeader("User-Agent", "USA TODAY Integration Client");

  	httppost.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));

   HttpResponse response = httpclient.execute(httppost);
   
   HttpEntity entity = response.getEntity();
      
      
   // If the response does not enclose an entity, there is no need
   // to bother about connection release
   if (entity != null) {
       InputStream instream = entity.getContent();
       try {
           BufferedReader rd = new BufferedReader(new InputStreamReader(instream));
           String line;
           while ((line = rd.readLine()) != null) {
               // Process line...
        	  // System.out.println(line);
               if (line.indexOf("Success") > -1) {
               	   //System.out.println("success");
               	   buf_writer.write( getDateTime() + " " ); 			        
                      buf_writer.write("Intergration Successful for Subscriber account# " + removeQuotes(A[22])); 
                      buf_writer.write( "  Name = " + removeQuotes(A[2]) + " " + removeQuotes(A[1])); 
                      buf_writer.newLine();  
                   break;
               }
           }
           rd.close();
           // do something useful with the response
       } catch (IOException ex) {
           // In case of an IOException the connection will be released
           // back to the connection manager automatically
           throw ex;
       } catch (RuntimeException ex) {
           // In case of an unexpected exception you may want to abort
           // the HTTP request in order to shut down the underlying
           // connection immediately.
           httppost.abort();
           throw ex;
       } finally {
           // Closing the input stream will trigger connection release
           try { instream.close(); } catch (Exception ignore) {}
       }        		 
	} 		
}
	    in.close();
	    
	
	    // File (or directory) with old name
	    File file = new File(processFile);
	    
	    // File (or directory) with new name
	    File file2 = new File(processedfile);
	    
	    // Rename file (or directory)
	    boolean renamed = file.renameTo(file2);
	    if (!renamed) {
	    	 buf_writer.newLine();
	    	 buf_writer.write( "***** Failed renaming file " + file + " to " + file2 + "*****"); 
	        buf_writer.newLine();
	    } else {
	    	 buf_writer.newLine();
	    	 buf_writer.write( "*** Completed renaming file " + file + " to " + file2 + "*****"); 
	        buf_writer.newLine();
	    } 
	  
	    buf_writer.close(); 	 
}
}
public static String removeQuotes(String s) {  		   
	int begIndex = s.indexOf('\"');
	int endIndex = s.lastIndexOf('\"');   
	String noQuotes = s.substring((begIndex + 1), endIndex);
	if (noQuotes.length() == 1) {
		return "";
	} else {
		return noQuotes;
	}
}		

public static String getDateFormatted() {		   	     
SimpleDateFormat bartDateFormat =
new SimpleDateFormat("EEEE-MMMM-dd-yyyy");	 
Date date = new Date();	  
return bartDateFormat.format(date);
}

public static Date getDateTime() {
Date date = new Date();
	return date;
}

}
