
package com.usatoday.nexternal.integration;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
public class oldProductionVersionNexternalIntegration {

//	Test Website
//	public static String nexternalURL = "https://www.nexternal.com/staging/default.asp";

//	Production Website
	public static String nexternalURL = "https://www.nexternal.com/usatoday/default.asp";
	
//  Location of Nexternal_Integration directory
	public static String base_URL = "D:\\Nexternal_Integration\\";
//	public static String base_URL = "C:\\Nexternal_Integration\\";

//  400 files
	public static String updateCustomerFile =  "C:\\inetpub\\ftproot\\nextftp\\outgoing\\Customer.txt";
	
public static void main(String[] args)throws IOException {	 {		
		 new StringBuffer();  
		 		 
		 boolean updFileExists = (new File(updateCustomerFile)).exists();
		 if (updFileExists) {
		        sendToNexternal(updateCustomerFile, "UPDATE");
		 } else {
		 	  new BufferedWriter(new FileWriter(base_URL + "Logs\\LogFile_For_Updates_Not_Found_"  + getDateFormatted() + ".txt"));
				
		 	
		 } 
		 
	 }		
}

public static void sendToNexternal(String processFile, String processType)throws IOException {	 {
	 
	 String processedfile = base_URL +"Processed_Files\\Completed_" + processType + "_" + getDateFormatted() + "_.txt";
	 String outfilename = base_URL + "Logs\\LogFile_For_" + processType + "_" + getDateFormatted() + ".txt"  ;
	
	 new StringBuffer(); 	   	
	 BufferedReader in = new BufferedReader(new FileReader(processFile));
	 BufferedWriter buf_writer = new BufferedWriter(new FileWriter(outfilename));
	 
  	 buf_writer.write( "        Nexternal Intergration for Website: " + nexternalURL + " File= " + processFile); 
	 buf_writer.newLine();
	 
	 while (true) {
	   String s = in.readLine();
	   if (s == null) {
		   break;
	   }
	   java.util.StringTokenizer stringTokenizer= new java.util.StringTokenizer(s,"||"); 
	   int index = 0; 
	   int numTokens = stringTokenizer.countTokens(); 
	   
	   String[] A = new String[numTokens]; 
	   if(numTokens>0) 
	    { 
	       while(stringTokenizer.hasMoreTokens()) 
	       { 
	          String token = stringTokenizer.nextToken(); 
	          A[index++] = token;
	       } 
	    } 
	  
	   StringBuffer data = new StringBuffer();
       data.append(URLEncoder.encode("LAST_NAME", "UTF-8")).append("=").append(URLEncoder.encode(removeQuotes(A[1]),"UTF-8"));
       data.append("&").append(URLEncoder.encode("FIRST_NAME", "UTF-8")).append("=").append(URLEncoder.encode(removeQuotes(A[2]),"UTF-8"));
       data.append("&").append(URLEncoder.encode("EMAIL", "UTF-8")).append("=").append(URLEncoder.encode(removeQuotes(A[3]),"UTF-8"));
       data.append("&").append(URLEncoder.encode("ADDRESS1", "UTF-8")).append("=").append(URLEncoder.encode(removeQuotes(A[5]),"UTF-8"));
       data.append("&").append(URLEncoder.encode("ADDRESS2", "UTF-8")).append("=").append(URLEncoder.encode(removeQuotes(A[6]),"UTF-8"));
       data.append("&").append(URLEncoder.encode("CITY", "UTF-8")).append("=").append(URLEncoder.encode(removeQuotes(A[7]),"UTF-8"));
       data.append("&").append(URLEncoder.encode("STATE", "UTF-8")).append("=").append(URLEncoder.encode(removeQuotes(A[8]), "UTF-8"));
       data.append("&").append(URLEncoder.encode("POSTAL_CODE","UTF-8")).append("=").append(URLEncoder.encode(removeQuotes(A[9]), "UTF-8"));
       data.append("&").append(URLEncoder.encode("COUNTRY", "UTF-8")).append("=").append(URLEncoder.encode(removeQuotes(A[10]), "UTF-8"));
       data.append("&").append(URLEncoder.encode("PHONE_NUM", "UTF-8")).append("=").append(URLEncoder.encode(removeQuotes(A[12]), "UTF-8")); // may need to format as above
       data.append("&").append(URLEncoder.encode("MAILING_LIST", "UTF-8")).append("=").append(URLEncoder.encode("0","UTF-8"));
       data.append("&").append(URLEncoder.encode("CUST_TYPE","UTF-8")).append("=").append(URLEncoder.encode(removeQuotes(A[19]), "UTF-8"));
       if (removeQuotes(A[19]) != null) {
       data.append("&").append(URLEncoder.encode("PASSWORD", "UTF-8")).append("=").append(URLEncoder.encode(removeQuotes(A[20]),"UTF-8"));
       }
       data.append("&").append(URLEncoder.encode("CUSTOM_FIELD1","UTF-8")).append("=").append(URLEncoder.encode(removeQuotes(A[22]),"UTF-8"));
       data.append("&").append(URLEncoder.encode("CUSTOM_FIELD2","UTF-8")).append("=").append(URLEncoder.encode(removeQuotes(A[23]), "UTF-8"));
       data.append("&").append(URLEncoder.encode("CUSTOM_FIELD3", "UTF-8")).append("=").append(URLEncoder.encode(removeQuotes(A[24]),"UTF-8"));
       data.append("&").append(URLEncoder.encode("CUSTOM_FIELD4","UTF-8")).append("=").append(URLEncoder.encode(removeQuotes(A[25]),"UTF-8"));
       data.append("&").append(URLEncoder.encode("CUSTOM_FIELD5","UTF-8")).append("=").append(URLEncoder.encode(removeQuotes(A[26]),"UTF-8"));
       data.append("&").append(URLEncoder.encode("CUSTOM_FIELD6","UTF-8")).append("=").append(URLEncoder.encode(removeQuotes(A[27]),"UTF-8"));
       data.append("&").append(URLEncoder.encode("Match1","UTF-8")).append("=").append(URLEncoder.encode("CUSTOM_FIELD1","UTF-8"));
       data.append("&").append(URLEncoder.encode("Mode","UTF-8")).append("=").append(URLEncoder.encode("Display-Only", "UTF-8"));
       data.append("&").append(URLEncoder.encode("Submit","UTF-8")).append("=").append(URLEncoder.encode("Shop Online","UTF-8"));
       	       
       URL url = new URL(nexternalURL);
       java.net.HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
       urlConnection.setDoInput(true);
       urlConnection.setDoOutput(true);
       urlConnection.setUseCaches(false);
       urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
       urlConnection.setRequestProperty("REFERER", "http://service.usatoday.com");       
       DataOutputStream dOut = new DataOutputStream(urlConnection.getOutputStream());
       dOut.writeBytes(data.toString());
       dOut.flush();
       dOut.close();
       
       BufferedReader rd = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
       String line;
       new StringBuffer();
       while ((line = rd.readLine()) != null) {
           // Process line...	
           buf_writer.write( getDateTime() + " " ); 
           buf_writer.write( "Integration Results for " + removeQuotes(A[22]) +  " = " + line); 
           buf_writer.newLine(); 
   	       if (line.indexOf("Success") > -1) {
               buf_writer.write( getDateTime() + " " ); 			        
               buf_writer.write("Intergration Successful for Subscriber account# " + removeQuotes(A[22])); 
               buf_writer.write( "  Name = " + removeQuotes(A[2]) + " " + removeQuotes(A[1])); 
               buf_writer.newLine(); 	           	
           } 
       }
       rd.close(); 		 
	}
	  in.close();
	   // File (or directory) with old name
	    File file = new File(processFile);
	    
	    // File (or directory) with new name
	    File file2 = new File(processedfile);
	    
	    // Rename file (or directory)
	    boolean renamed = file.renameTo(file2);
	    if (!renamed) {
	    	 buf_writer.newLine();
	    	 buf_writer.write( "***** Failed renaming file " + file + " to " + file2 + "*****"); 
            buf_writer.newLine();
	    } else {
	    	 buf_writer.newLine();
	    	 buf_writer.write( "*** Completed renaming file " + file + " to " + file2 + "*****"); 
            buf_writer.newLine();
	    }
	  buf_writer.close(); 			
}
}

public static String removeQuotes(String s) {  		   
	int begIndex = s.indexOf('\"');
	int endIndex = s.lastIndexOf('\"');   
	String noQuotes = s.substring((begIndex + 1), endIndex);
	if (noQuotes.length() == 1) {
		return "";
	} else {
		return noQuotes;
	}
}		

public static String getDateFormatted() {		   	     
SimpleDateFormat bartDateFormat =
new SimpleDateFormat("EEEE-MMMM-dd-yyyy");	 
Date date = new Date();	  
return bartDateFormat.format(date);
}

public static Date getDateTime() {
Date date = new Date();
	return date;
}

}
